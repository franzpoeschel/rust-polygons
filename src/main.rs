use image::{GenericImage, GenericImageView, ImageResult, Pixel, Rgba};
use rand::SeedableRng;
use std::env;
use std::f64::consts::PI;
use std::iter::Sum;
use std::str;

extern crate image;
extern crate rand;

enum Method {
    Lines,
    Circles,
}

struct Config {
    src: String,
    dest: String,
    method: Method,
    repetitions: u32,
}

fn parse_args() -> Option<Config> {
    let (a1, a2, a3, a4) = match &env::args().collect::<Vec<_>>()[..] {
        [_, a1, a2, a3, a4] => (a1.clone(), a2.clone(), a3.clone(), a4.clone()),
        _ => {
            return None;
        }
    };
    let method = match &a3.to_lowercase() as &str {
        "lines" => Method::Lines,
        "circles" => Method::Circles,
        _ => {
            return None;
        }
    };
    let repetitions = a4.parse::<u32>().ok()?;
    Some(Config {
        src: a1,
        dest: a2,
        method,
        repetitions,
    })
}

fn main() {
    let config = parse_args().unwrap_or_else(|| {
        panic!("Some error in arguments.");
    });
    recreate_image_io(config).unwrap_or_else(|e| {
        eprintln! {"{:?}", e};
    });
}

mod helpers {
    use image::{GenericImage, Pixel, Primitive, Rgba};
    use std::iter::Sum;

    /*******************************************************
     * Define a trait to define how similar two pixels are *
     *******************************************************/
    pub trait Similarity {
        type R;
        fn similarity(&self, b: &Self) -> Self::R;

        fn stream_similarity<S>(stream: S) -> Self::R
        where
            Self: Sized,
            S: IntoIterator<Item = (Self, Self)>,
            <Self as Similarity>::R: Sum,
        {
            stream
                .into_iter()
                .map(|(ref x, ref y)| x.similarity(y))
                .sum()
        }
    }

    impl Similarity for u8 {
        type R = u32;
        fn similarity(&self, b: &Self) -> Self::R {
            let diff = u32::from(*self) - u32::from(*b);
            diff * diff
        }
    }

    impl<T> Similarity for Rgba<T>
    where
        T: Primitive + Copy + Similarity,
        <T as Similarity>::R: Sum,
    {
        type R = <T as Similarity>::R;
        fn similarity(&self, b: &Self) -> Self::R {
            let v1 = [self[0], self[1], self[2], self[3]];
            let v2 = [b[0], b[1], b[2], b[3]];
            let is = v1.iter().cloned();
            let is2 = v2.iter().cloned();
            Similarity::stream_similarity(is.zip(is2))
        }
    }

    pub fn comp_images<I, I2, P, R>(i1: &I, i2: &I2, range: &[(u32, u32)]) -> R
    where
        I: GenericImage<Pixel = P>,
        I2: GenericImage<Pixel = P>,
        P: Pixel + Similarity<R = R>,
        R: Sum,
    {
        Similarity::stream_similarity(
            range
                .iter()
                .map(|&(x, y)| (i1.get_pixel(x, y), i2.get_pixel(x, y))),
        )
    }

    pub struct CandidateShape<Pxl> {
        pub color: Pxl,
        pub shape: Vec<(u32, u32)>,
    }

    pub fn comp_image_to_candidate_shape<SourceImage, Pxl>(
        i1: &SourceImage,
        candidate_shape: &CandidateShape<Pxl>,
    ) -> <Pxl as Similarity>::R
    where
        SourceImage: GenericImage<Pixel = Pxl>,
        Pxl: Pixel + Similarity,
        <Pxl as Similarity>::R: Sum,
    {
        let CandidateShape { color, shape } = candidate_shape;
        shape
            .iter()
            .map(|&(x, y)| i1.get_pixel(x, y).similarity(color))
            .sum()
    }
}

use helpers::Similarity;

fn recreate_image_io(config: Config) -> ImageResult<()> {
    let Config {
        src,
        dest,
        method,
        repetitions,
    } = config;
    let src = image::open(src)?;
    let mut i1 = src.clone();
    {
        let (x, y) = i1.dimensions();
        for i in 0..x {
            for j in 0..y {
                i1.put_pixel(i, j, Rgba::<u8>([0, 0, 0, 0]));
            }
        }
    }
    let rng = rand::StdRng::from_seed(&[
        32649, 20200, 173, 31387, 18095, 24051, 23928, 4703, 27639, 25254,
        26672, 30381, 31342, 2501, 25837, 29921, 28703, 10732, 28823, 16122,
        26351, 4972, 17159, 18204, 21700, 10611, 22071, 14013, 6028, 25022,
        13504, 1691,
    ]);

    let mut img_polygon: Box<dyn ImagePolygonCreatorTrait<_, _, _, _>> =
        match method {
            Method::Lines => Box::new(LineImagePolygonCreator {
                image_polygon: ImagePolygon {
                    src_image: src,
                    dest_image: i1,
                    rng,
                },
                line_width: 20,
            }),
            Method::Circles => Box::new(CircleImagePolygonCreator {
                image_polygon: ImagePolygon {
                    src_image: src,
                    dest_image: i1,
                    rng,
                },
                radius_range: (3, 8),
            }),
        };
    img_polygon.recreate_image(repetitions);
    img_polygon.get_data().dest_image.save(dest)
}

/*****************************************************
 * Struct for the main data needed during processing *
 *****************************************************/

struct ImagePolygon<I, I2, Rng> {
    src_image: I,
    dest_image: I2,
    rng: Rng,
}

impl<SourceImage: GenericImage, TargetImage, Rng: rand::Rng>
    ImagePolygon<SourceImage, TargetImage, Rng>
{
    fn random_point(&mut self) -> (u32, u32) {
        let (a1, b1, a2, b2) = self.src_image.bounds();
        (self.rng.gen_range(a1, a2), self.rng.gen_range(b1, b2))
    }
}

/******************************************************
 * Trait for the configurable part of the computation *
 ******************************************************/

trait ImagePolygonCreatorTrait<SourceImage, TargetImage, Rng, Pxl>
where
    SourceImage: GenericImage<Pixel = Pxl>,
    TargetImage: GenericImage<Pixel = Pxl>,
    Pxl: Pixel + Similarity,
    <Pxl as Similarity>::R: Ord + Sum,
    Rng: rand::Rng,
{
    fn get_data(&self) -> &ImagePolygon<SourceImage, TargetImage, Rng>;
    fn get_data_mut(
        &mut self,
    ) -> &mut ImagePolygon<SourceImage, TargetImage, Rng>;
    fn get_shape(&mut self) -> Vec<(u32, u32)>;

    fn generate_polygon(&mut self) {
        use helpers::CandidateShape;

        let sample_pixel = |_self: &mut Self| -> Pxl {
            let data = _self.get_data_mut();
            let i = &data.src_image;
            let (x1, y1, x2, y2) = i.bounds();
            i.get_pixel(data.rng.gen_range(x1, x2), data.rng.gen_range(y1, y2))
        };

        let candidate_shape = CandidateShape {
            color: sample_pixel(self),
            shape: self.get_shape(),
        };

        let fill_shape = |_self: &mut Self| {
            let CandidateShape { color, ref shape } = candidate_shape;
            let data = _self.get_data_mut();
            for &(x, y) in shape.iter() {
                data.dest_image.put_pixel(x, y, color);
            }
        };

        use helpers::comp_image_to_candidate_shape;
        use helpers::comp_images;

        let data = self.get_data_mut();

        if comp_image_to_candidate_shape(&data.src_image, &candidate_shape)
            <= comp_images(
                &data.src_image,
                &data.dest_image,
                &candidate_shape.shape,
            )
        {
            fill_shape(self);
        }
    }

    fn recreate_image(&mut self, n: u32) {
        for _ in 0..n {
            self.generate_polygon();
        }
    }
}

struct LineImagePolygonCreator<SourceImage, TargetImage, Rng> {
    image_polygon: ImagePolygon<SourceImage, TargetImage, Rng>,
    line_width: u32,
}

impl<SourceImage, TargetImage, Rng, Pxl>
    ImagePolygonCreatorTrait<SourceImage, TargetImage, Rng, Pxl>
    for LineImagePolygonCreator<SourceImage, TargetImage, Rng>
where
    SourceImage: GenericImage<Pixel = Pxl>,
    TargetImage: GenericImage<Pixel = Pxl>,
    Pxl: Pixel + Similarity,
    <Pxl as Similarity>::R: Ord + Sum,
    Rng: rand::Rng,
{
    fn get_data(&self) -> &ImagePolygon<SourceImage, TargetImage, Rng> {
        &self.image_polygon
    }
    fn get_data_mut(
        &mut self,
    ) -> &mut ImagePolygon<SourceImage, TargetImage, Rng> {
        &mut self.image_polygon
    }
    fn get_shape(&mut self) -> Vec<(u32, u32)> {
        let lw = self.line_width;
        let data = self.get_data_mut();
        let (a1, b1, a2, b2) = data.src_image.bounds();
        let (x, y) = data.random_point();
        let m = data.rng.gen_range::<f64>(-PI / 2.0, PI / 2.0).tan();
        let t = f64::from(y) - m * f64::from(x);
        let dx = ((f64::from(lw) * m.abs()) / (m * m + 1.0).sqrt()) as u32;
        let x1 = (if x < lw { 0 } else { x - dx }).max(a1);
        let x2 = (x + dx).min(a2 - 1);
        (x1..x2 + 1)
            .map(|x| (x, (m * f64::from(x) + t) as u32))
            .filter(|&(_, y)| b1 <= y && y < b2)
            .collect()
    }
}

struct CircleImagePolygonCreator<SourceImage, TargetImage, Rng> {
    image_polygon: ImagePolygon<SourceImage, TargetImage, Rng>,
    radius_range: (u32, u32),
}

impl<SourceImage, TargetImage, Rng, Pxl>
    ImagePolygonCreatorTrait<SourceImage, TargetImage, Rng, Pxl>
    for CircleImagePolygonCreator<SourceImage, TargetImage, Rng>
where
    SourceImage: GenericImage<Pixel = Pxl>,
    TargetImage: GenericImage<Pixel = Pxl>,
    Pxl: Pixel + Similarity,
    <Pxl as Similarity>::R: Ord + Sum,
    Rng: rand::Rng,
{
    fn get_data(&self) -> &ImagePolygon<SourceImage, TargetImage, Rng> {
        &self.image_polygon
    }
    fn get_data_mut(
        &mut self,
    ) -> &mut ImagePolygon<SourceImage, TargetImage, Rng> {
        &mut self.image_polygon
    }
    fn get_shape(&mut self) -> Vec<(u32, u32)> {
        let (a, b) = self.radius_range;
        let data = self.get_data_mut();
        let radius = data.rng.gen_range(a, b) as i32;
        let (a1, b1, a2, b2) = data.src_image.bounds();
        let (x, y) = data.random_point();
        let x1 = (-radius).max(a1 as i32 - x as i32);
        let x2 = radius.min(a2 as i32 - x as i32);
        (x1..x2)
            .flat_map(|dx| {
                let dy = f64::from(radius * radius - dx * dx).sqrt() as u32;
                let y_lower = if y < b1 + dy { b1 } else { y - dy };
                let y_upper = (y + dy).min(b2 - 1);
                (y_lower..y_upper + 1).map(move |y| ((x as i32 + dx) as u32, y))
            })
            .collect()
    }
}
